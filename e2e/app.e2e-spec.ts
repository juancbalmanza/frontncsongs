import { NCSongsPage } from './app.po';

describe('ncsongs App', () => {
  let page: NCSongsPage;

  beforeEach(() => {
    page = new NCSongsPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});

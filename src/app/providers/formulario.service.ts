import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, ResponseContentType } from '@angular/http';

import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/catch';

const BASE_URL = 'http://localhost:9000/';

@Injectable()
export class FormularioService {

  /**
     * Campos del servicio.
     */
    private Jsons: any; // Variable que contiene el json con los datos a enviar.
    private headers = new Headers({ 'Content-Type': 'application/json' }); // Constante que asigna los valores necesarios a los headers para realizar la transacción.
    private options = new RequestOptions({ headers: this.headers }); // Constante que contiene las opciones necesarias para realizar una transacción con el backend.

    /**
     * Constructor del servicio.
     * @param {Http} http Variable que permite la comunicacion con el servidor.
     */
    constructor( private http: Http ) { }

    /**
     * Función que permite realizar algún CUD en la base de datos a travez del backend.
     * @param {string} CUDUrl Variable que contiene el complemento de la url a direccionar hacia el backend.
     * @param {any} valor Variable que contiene los datos para generar el json.
     * @return Retorna el mensaje de confirmación recibido del backend (error u ok).
     */
    CRUD(entidad: string, valor: any, accion: string, id: any) {
        this.Jsons = JSON.stringify(valor);
        switch (accion) {
          case 'agregar':
              return this.http.post(BASE_URL + accion + entidad, this.Jsons, this.options)
                .map(r => r.json())
                .catch(this.handleError);
          case 'eliminar' && 'actualizar':
              return this.http.patch(BASE_URL + accion + entidad + "/'" + id + "'", this.Jsons, this.options)
                .map(r => r.json())
                .catch(this.handleError);
          case 'consultar':
              return this.http.get(BASE_URL + accion + entidad + "/'" + id + "'", this.options)
                .map(r => r.json())
                .catch(this.handleError);
          case 'lista':
              return this.http.get(BASE_URL + accion + entidad, this.options)
                .map(r => r.json())
                .catch(this.handleError);
          default:
            break;
        }

        return this.http.post(BASE_URL + entidad, this.Jsons, this.options)
            .map(r => r.json())
            .catch(this.handleError);
    }

    consultar(entidad: string, valor: any, id: any) {
        this.Jsons = JSON.stringify({valor});
        return this.http.get(BASE_URL + entidad + "/'" + id + "'")
          .map((response: Response) => response.json());
    }

    eliminar (entidad: string, id: any) {
        return this.http.delete(BASE_URL + entidad + "/'" + id + "'")
          .map((response: Response) => response.json());
    }

    actualizar(entidad: string, valor: any, id: any) {
        this.Jsons = JSON.stringify({valor});
        return this.http.patch(BASE_URL + entidad + "/'" + id + "'", this.Jsons, this.options)
          .map((response: Response) => response.json());
    }

    /**
     * Función que dá formato al error obtenido desde el backend.
     * @param {any} error Variable que contiene toda la informacion del error generado desde el backend.
     */
    private handleError(error: Response | any) {
        let errMsg: string;
        if ( error instanceof Response ) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        return Observable.throw(errMsg);
    }

}

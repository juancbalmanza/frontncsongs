import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  /**
   * Contructor del componente.
   * @param {Router} router Variable que permite la navegación entre páginas de la aplicación.
   * @param {ActivatedRoute} activatedRoute Variable que nos permite acceder a los parámetros de la url desde el componente.
   * @param {FormBuilder} fb Variable usada para la gestión de los controles de formularios.
   * @param {VistasService} serviceForm Servicio que permite la comunicación con el backend.
   */
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    ) {}

}

import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { FormularioService } from '../../providers/formulario.service';

@Component({
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent {
    model: any = {};
    loading = false;

    constructor(
        private router: Router,
        private FService: FormularioService) {
          this.FService.CRUD('Album', '', 'lista', '')
            .subscribe(
                data => { console.log(data);
                })
    }

    register() {
        this.loading = true;
        this.FService.CRUD('Tercero', this.model, 'agregar', '')
            .subscribe(
                data => {
                    this.router.navigate(['/login']);
                },
                error => {
                    this.loading = false;
                });
    }

}
